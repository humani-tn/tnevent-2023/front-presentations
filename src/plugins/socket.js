import { writable } from "svelte/store";
import ReconnectingWebSocket from 'reconnecting-websocket';

let s = import.meta.env.VITE_WEBSOCKET;
console.debug("connecting to", s);
let socket;
// let intervalId;
const subscribers = new Set();
connect();

function connect() {
  socket = new ReconnectingWebSocket(s);

  // socket.addEventListener("open", () => {
  //   clearInterval(intervalId);
  // });

  socket.addEventListener("message", event => {
    subscribers.forEach(subscriber => {
      subscriber(event.data)
    });
  });

  // socket.addEventListener("close", () => {
  //   // close previous socket
  //   socket.close();
  //   socket = null;
  //   // try to reconnect in 1-5 seconds
  //   // const random = Math.floor(Math.random() * 4000) + 1000;
  //   // intervalId = setTimeout(connect, random);
  // });
}


export const socketStore = writable({
  messages: [],
  send: async message => {
    if (socket) {
      while (socket.readyState === ReconnectingWebSocket.CONNECTING) {
        console.log("waiting for websocket to connect");
        // wait 10ms 
        await new Promise(resolve => setTimeout(resolve, 100));
      }
      socket.send(message);
    }
  },
  subscribe: subscriber => {
    subscribers.add(subscriber);

    return () => {
      subscribers.delete(subscriber);
    };
  }
});

