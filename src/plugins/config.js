export let team_colors = {
  0: "blue-600", // Bleu
  1: "red-600", // Rouge
  2: "orange-600", // Orange
  3: "purple-600", // Violet
  4: "base-300", // Principal
};

export let team_gradient_colors = {
  0: "to-blue-600", // Bleu
  1: "to-red-600", // Rouge
  2: "to-orange-600", // Orange
  3: "to-purple-600", // Violet
  4: "to-base-300", // Principal
};

export let team_bg_colors = {
  0: "bg-blue-400", // Bleu
  1: "bg-red-400", // Rouge
  2: "bg-orange-400", // Orange
  3: "bg-purple-400", // Violet
  4: "bg-base-300", // Principal
};

export let team_names = {
  0: "blublublbublublbll",
  1: "FEUR 🏒", // FEUR <emoji hockey sur glace>
  2: "Diving Vector",
  3: "Lila Legends"
};

export let team_indicators = {
  0: "♦",
  1: "◼",
  2: "▲",
  3: "●",
  4: "",
}