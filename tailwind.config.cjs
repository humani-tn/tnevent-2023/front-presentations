/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')
module.exports = {
  content: [
    './src/**/*.{html,js,svelte,ts}',
  ],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'blueteam': '#5b99c7',
        'redteam': '#fe8325',
        'orangeteam': '#fe8325',
        'purpleteam': '#5b4886' 
      }
    },
  },
  plugins: [
    require('daisyui')
  ],
  daisyui: {
    themes: [
      "cupcake",
    ],
  },
}
