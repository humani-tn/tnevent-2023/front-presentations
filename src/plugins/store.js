import { writable } from 'svelte/store'
import { socketStore } from '@/plugins/socket.js';

// 0 => not fetched yet
// 1 => not started
// 2 => started
// 3 => finished
// 4 => undefined
export const started_state = writable(0)
// export const finished_state = writable(0)

export const associations = writable([])

// The global pot is updated by the layout
export const global_pot = writable(0);

function createTopicsStore() {
    const {subscribe, set, update} = writable([]);

    return {
        subscribe,
        sub: topic => {

            console.debug("Subscribed to " + topic);
    
            // Add the topic to the store
            update((store) => {
                store.push(topic);
                return store;
            });
    
            // Subscribe the socket to the tocpic if this is the first subscriber
            socketStore.update(store => {
                store.send(JSON.stringify({"op": "sub", "msg": topic}));
                return store;
            });
        },
        unsub: topic => {
    
            console.debug("Unsubscribed from " + topic);

            let lastSub;

            // Remove the topic from the store
            update((store) => {
                store.splice(store.indexOf(topic), 1);
                lastSub = !(store.includes(topic));
                return store;
            });
    
            // Unsubscribe the socket from the tocpic if there are no more subscribers
            if (lastSub) {
                socketStore.update(store => {
                    store.send(JSON.stringify({"op": "unsub", "msg": topic}));
                    return store;
                });
            }
        }
    }
}

// topic store for the websocket
export const topics = createTopicsStore()