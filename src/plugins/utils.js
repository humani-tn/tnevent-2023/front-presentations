export function splitIntoDigits(num, precision) {
    let digits;
    // convert num i nto float with 2 decimals
    num = parseFloat(num).toFixed(precision);
    digits = num.toString().split('');
    // Add a . every 3 digits (ignoring the decimals)
    if (precision > 0) {
        for (let i = digits.length - 3 - (precision+1); i > 0; i -= 3) {
            digits.splice(i, 0, ' ');
        }
    } else {
        for (let i = digits.length - 3; i > 0; i -= 3) {
            digits.splice(i, 0, ' ');
        }
    }
    return digits
}